import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
import matplotlib.pyplot as plt
import seaborn as sns
import joblib

music_data = pd.read_csv('music.csv')

X = music_data.drop(columns=['genre'])
y = music_data['genre']

model = joblib.load('music_model.joblib')

predictions = model.predict(X)
accuracy = accuracy_score(y, predictions)
print("Accuracy:", accuracy)

f1 = f1_score(y, predictions, average='weighted')
print("F1 Score:", f1)

conf_matrix = confusion_matrix(y, predictions)
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=model.classes_, yticklabels=model.classes_)
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.title('Confusion Matrix')
plt.savefig('confusion_matrix.png')

with open('metrics.txt', 'w') as f:
    f.write(f"\nAccuracy: {accuracy:.2f}\n")
    f.write(f"\nF1 Score: {f1:.2f}\n")