import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import joblib

# Загрузка данных
music_data = pd.read_csv('music.csv')

X = music_data.drop(columns=['genre'])
y = music_data['genre']

# Обучение модели
model = DecisionTreeClassifier()
model.fit(X, y)

# Сохранение модели в файл
joblib.dump(model, 'music_model.joblib')